terraform {
  experiments = [ module_variable_optional_attrs ]

  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.11"
    }

    local = {
      source = "hashicorp/local"
      version = "2.2.3"
    }

    ct = {
      source = "poseidon/ct"
      version = "0.11.0"
    }

    null = {
      source = "hashicorp/null"
      version = "3.1.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}
