terraform {
    backend "http" {
    }
}

terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.11"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }

    rancher2 = {
      source = "rancher/rancher2"
      version = "1.24.2"
    }

    time = {
      source = "hashicorp/time"
      version = "0.8.0"
    }

    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.18.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.8.2"
    }
  }
}

provider "proxmox" {
  pm_api_url = "${var.proxmox_server}/api2/json"
  pm_tls_insecure = var.proxmox_insecure

  pm_user = var.proxmox_credentials.username
  pm_password = var.proxmox_credentials.password

  #pm_api_token_id = var.proxmox_api_token
  #pm_api_token_secret = var.proxmox_api_secret
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "gitlab" {
  # Token will be automatically sourced from the proper environment variable
}

# This is the "seed" for the entire project; when this changes, the rancher-server
# will be recreated and thus all child clusters will be as well. TAKE CARE!!!
resource "random_uuid" "project-seed" {
}
