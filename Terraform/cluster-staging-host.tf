locals {
  staging-rancher-flatcar-primary-ip = "10.4.1.111"
}

# Add cluster node
module "staging-rancher-flatcar" {
    source = "./modules/proxmox-flatcar-vm"
    replace_on = rancher2_cluster_v2.cluster-staging-rancher.id
    depends_on = [
        cloudflare_record.staging-rancher-internal
    ]

    provisioning_config = {
        ssh = var.proxmox_ssh_config
        additional_configs = [
            # Deploy RKE2
            templatefile("${path.module}/resources/unit-rke-install.yaml", {
                command = "${rancher2_cluster_v2.cluster-staging-rancher.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker --node-name staging-flatcar"
            }),

            # Install GitLab Cluster Agent within cluster
            templatefile("${path.module}/resources/unit-rke-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.staging-agent-token.token
                })
            })
        ]
    }

    id = 412
    name = "staging-rancher"
    description = "Staging cluster node running flatcar OS"
    node = "centurion"
    template = "template-flatcar"
    memory = {
        maximum = 16324
    }
    core_count = 8

    networks = [
        {
            bridge_name = "vmbr41"
            address = "${local.staging-rancher-flatcar-primary-ip}"
            range = "/24"
            gateway = "10.4.1.2"
            dns = "1.1.1.1"
        },
        {
            bridge_name = "vmbr91"
            address = "10.9.1.114"
            range = "/24"
        },
        {
            bridge_name = "vmbr92"
            address = "10.9.2.114"
            range = "/24"
        }
    ]

    drives = [{
        size = "64G"
        backing_storage = "local-zfs"
    }]

    autostart = {
        timeout = 120
    }
}

# Add DNS registration
resource "cloudflare_record" "staging-rancher-public" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.staging"
    type = "CNAME"
    value = "hyperbabylon.fmf.nl"
}

resource "cloudflare_record" "staging-rancher-internal" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.staging.internal"
    type = "A"
    value = local.staging-rancher-flatcar-primary-ip
}
