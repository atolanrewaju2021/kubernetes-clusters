# Register Rancher cluster for staging
resource "rancher2_cluster_v2" "cluster-staging-rancher" {
    lifecycle {
        replace_triggered_by = [
            rancher2_bootstrap.rancher-server-bootstrap
        ]
    }
    
    # Basic cluster information
    name = "staging"
    kubernetes_version = "v1.24.4+rke2r1" # K8s 1.24 with RKE2
    default_cluster_role_for_project_members = "owner"

    # More detailed configuration
    enable_network_policy = false

    rke_config {
        machine_global_config = <<EOF
            cni: "calico"
        EOF

        etcd {
            snapshot_schedule_cron = "0 */5 * * *"
            snapshot_retention = 5
        }
    }

    local_auth_endpoint {
        enabled = true
        fqdn = cloudflare_record.staging-rancher-internal.hostname
    }
}

output "command" {
    value = rancher2_cluster_v2.cluster-staging-rancher.cluster_registration_token
    sensitive = true
}
