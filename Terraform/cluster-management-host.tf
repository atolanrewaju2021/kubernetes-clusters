locals {
    rancher-server-flatcar-primary-ip = "10.9.1.103"
}

module "rancher-server-flatcar" {
    source = "./modules/proxmox-flatcar-vm"
    replace_on = random_uuid.project-seed.result
    depends_on = [
        cloudflare_record.rancher-server-internal
    ]

    provisioning_config = {
        ssh = var.proxmox_ssh_config
        additional_configs = [
            # Install K3S
            file("${path.module}/resources/unit-k3s-install.yaml"),

            # Install Cert Manager within K3S
            templatefile("${path.module}/resources/unit-k3s-crd.yaml", {
                name = "cert-manager.yaml"
                content = file("${path.module}/resources/k8s-crd-cert-manager.yaml")
            }),

            # Install Rancher Server within K3S
            templatefile("${path.module}/resources/unit-k3s-crd.yaml", {
                name = "rancher.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-rancher.yaml", {
                    initial_password = random_password.rancher-server-initial-password.result
                    hostname = cloudflare_record.rancher-server-internal.hostname
                })
            }),

            # Install GitLab Cluster Agent within K3S
            templatefile("${path.module}/resources/unit-k3s-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.rancher-server-agent-token.token
                })
            })
        ]
    }

    id=916
    name = "rancher-server"
    description = "Flatcar OS VM running the Rancher Server"
    node = "centurion"
    template = "template-flatcar"
    memory = {
        maximum = 4096
    }
    core_count = 2

    networks = [
        {
            bridge_name = "vmbr91"
            address = "${local.rancher-server-flatcar-primary-ip}"
            range = "/24"
            gateway = "10.9.1.1"
            dns = "1.1.1.1"
        }
    ]

    drives = [{
        size = "64G"
        backing_storage = "local-zfs"
    }]

    autostart = {
        order = 3
    }
}

# Add DNS registration
resource "cloudflare_record" "rancher-server-internal" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "management.internal"
    type = "A"
    value = local.rancher-server-flatcar-primary-ip
}
