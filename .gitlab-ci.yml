# Include CI templates
include:
  - project: "fmfnl/gitlab-ci-templates"
    ref: master
    file:
      - config.yaml
      - selectors.yaml
      - runners.yaml

# Setting some variables and other info
variables:
  TF_ROOT: ${CI_PROJECT_DIR}/Terraform
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/machines
  TF_CLI_CONFIG_FILE: ${CI_PROJECT_DIR}/Terraform/.terraformrc

  VAULT_ROLE: infra_kubernetes_clusters
  VAULT_ENV: production

stages:
  - Preparation
  - Validation
  - Build & Plan
  - Deployment
  - Provisioning

# Workflow settings: only run pipeline on master, develop and PR
workflow:
  rules:
    - !reference [.workflow-rule-master]
    - !reference [.workflow-rule-develop]
    - !reference [.workflow-rule-mr]
    - !reference [.workflow-rule-default-no]

# Default stage settings
default:
  image: registry.gitlab.com/fmfnl/docker-images/gitlab-terraform-vault:latest
  before_script:
    - export VAULT_ADDR=https://vault.fmf.nl
    - cd ${TF_ROOT}
    - echo -e "credentials \"gitlab.com\" {\n  token = \"${CI_JOB_TOKEN}\"\n}" > ${TF_CLI_CONFIG_FILE}

  cache:
    key: terraform-data
    paths:
      - ${TF_ROOT}/.terraform

  tags: !reference [.on-linux, tags]

### STAGES
init:
  stage: Preparation
  script:
    - gitlab-terraform init

validate:
  stage: Validation
  script:
    - gitlab-terraform validate

plan:
  stage: Build & Plan

  script:
    # First we obtain a vault token and decrypt the secrets
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt-gitlab/login role=${VAULT_ROLE}-${VAULT_ENV} jwt=${CI_JOB_JWT})"
    - sops -d secrets.production.auto.tfvars.enc.json > secrets.production.auto.tfvars.json
    - touch provisioning_key && touch provisioning_key-cert.pub
    - gitlab-terraform plan
    - gitlab-terraform plan-json

  # Store the full plan _including secrets_ as an artifact for the next job to reuse.
  # For safety, we automatically remove the plan after an hour. Make sure to never
  # allow artifacts to be publicly visible.
  artifacts:
    name: plan
    expire_in: 1 hr
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

# Separate apply job only on master with manual trigger,
# as it can be a destructive action.
deploy:
  stage: Deployment
  environment:
    name: production
  dependencies:
    - plan
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual

  script:
    # First we will generate an SSH keypair and sign it using vault
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt-gitlab/login role=${VAULT_ROLE}-${VAULT_ENV} jwt=${CI_JOB_JWT})"
    - ssh-keygen -b 4096 -t rsa -f provisioning_key -q -N ""
    # NOTE that this signed key has a TTL of 5 minutes by Vault policies
    - vault write -field=signed_key ssh/sign/ci public_key=@provisioning_key.pub > provisioning_key-cert.pub

    # Second, set up an SSH SOCKS proxy to our infra so we can use internal addresses
    # The proxy host will be provided by GitLab, but we still need to trust the host CA
    - mkdir ~/.ssh
    - echo "@cert-authority *.fmf.nl,*.comcie.nl $(curl "$VAULT_ADDR/v1/ssh/public_key")" >> ~/.ssh/known_hosts
    - ssh -D 5678 -4 -N -i provisioning_key -i provisioning_key-cert.pub ${PROXY_HOST} &
    - export HTTPS_PROXY=socks5://localhost:5678

    # And then apply the plan as made in the previous stage - no re-decryption of secrets necessary
    - gitlab-terraform apply || exit_code=$?

    # We terminate the proxy and return the exit code of the apply operation
    - kill %1
    - exit $exit_code

# Triggering kubernetes infrastructure deployment if relevant as
# a multi-project pipeline
provision:
  stage: Provisioning
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual

  parallel:
    matrix:
      # We cannot name this "BRANCH" since that will conflict with the template selectors we use
      - DOWNSTREAM_BRANCH: management
      - DOWNSTREAM_BRANCH: production
      - DOWNSTREAM_BRANCH: staging
      - DOWNSTREAM_BRANCH: develop

  trigger:
    project: fmfnl/infrastructure/kubernetes-infrastructure
    branch: $DOWNSTREAM_BRANCH
    strategy: depend
  
  inherit:
    variables: false
